package com.teste.prototyperecognition;

import org.jetbrains.annotations.Nullable;

public final class Native {

    public static native String stringFromJNI();

    public static native void loadModel(final String path);

    public static native int trainObjectDetector(final String pathDetector, String imgPath);
}
