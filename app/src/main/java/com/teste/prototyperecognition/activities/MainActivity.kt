package com.teste.prototyperecognition.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.teste.prototyperecognition.Native
import com.teste.prototyperecognition.databinding.ActivityMainBinding
import java.io.File

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val RESOURCE_PATH = "src/main/java/resources/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Example of a call to a native method
        binding.sampleText.text = Native.stringFromJNI()

//        Native.loadModel( RESOURCE_PATH+"detector_vacas_faces.svm")

        Native.trainObjectDetector(RESOURCE_PATH+"detector_vacas_nose_pontos.dat","src/main/java/images")
//        Native.trainObjectDetector(RESOURCE_PATH+"detector_vacas_faces_v13_cel.svm","src/main/java/images")

    }

    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")

        }
    }
}