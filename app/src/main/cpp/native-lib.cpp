#include <jni.h>
#include <string>

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <android/log.h>
#include <mutex>

#include <dlib/image_processing.h>
#include <dlib/image_processing/generic_image.h>
#include <dlib/opencv/cv_image.h>
#include <dlib/dnn.h>
#include <dlib/image_io.h>
#include <dlib/gui_widgets.h>
#include <dlib/data_io/load_image_dataset.h>
#include <dlib/svm_threaded.h>
#include <cstring>
#include <dlib/cmd_line_parser.h>

#include <dlib/serialize.h>


//#include <dlib/dlib/dir_nav.h>
//#include <dlib/dlib/data_io.h>


#define LOG_TAG "native-lib"
#define LOGD(...) \
  ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

#define JNI_METHOD(NAME) \
    Java_com_teste_prototyperecognition_Native_##NAME

//using namespace dlib;
using namespace std;
using namespace dlib;
namespace fs = std::__fs::filesystem;
using fs::recursive_directory_iterator;

typedef scan_fhog_pyramid<pyramid_down<6> > image_scanner_type;
template <long num_filters, typename SUBNET> using con5d = con<num_filters,5,5,2,2,SUBNET>;
template <long num_filters, typename SUBNET> using con5  = con<num_filters,5,5,1,1,SUBNET>;
template <typename SUBNET> using downsampler  = relu<affine<con5d<32, relu<affine<con5d<32, relu<affine<con5d<16,SUBNET>>>>>>>>>;
template <typename SUBNET> using rcon5  = relu<affine<con5<55,SUBNET>>>;
using net_type = loss_mmod<con<1,9,9,1,1,rcon5<rcon5<rcon5<downsampler<input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;

extern "C" JNIEXPORT jstring JNICALL
JNI_METHOD(stringFromJNI)(
        JNIEnv *env,
        jclass clazz) {
    std::string hello = "Hello from C++";

    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT void JNICALL
JNI_METHOD(loadModel)(JNIEnv *env, jclass, jstring detectorPath) {
//    try {
//
//        const char *path = env->GetStringUTFChars(detectorPath, JNI_FALSE);
//        LOGD("JNI: path -> %s", path);
//
//        // load the shape predictor
//        deserialize(path) >> m_shape_predictor;
//
//        image_dataset_metadata::dataset data;
//        load_image_dataset_metadata(data, dataset_filename);
//
//        env->ReleaseStringUTFChars(detectorPath, path); //free mem
//        LOGD("JNI: model loaded");
//
//    } catch (dlib::serialization_error &e) {
//        LOGD("JNI: failed to model -> %s", e.what());
//    }
}

// ----------------------------------------------------------------------------------------

std::string jstring2string(JNIEnv *env, jstring jStr) {
    if (!jStr)
        return "";

    const jclass stringClass = env->GetObjectClass(jStr);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(jStr, getBytes,
                                                                       env->NewStringUTF("UTF-8"));

    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte *pBytes = env->GetByteArrayElements(stringJbytes, NULL);

    std::string ret = std::string((char *) pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}

// ----------------------------------------------------------------------------------------

extern "C"
JNIEXPORT int JNICALL
JNI_METHOD(trainObjectDetector)(JNIEnv *env, jclass, jstring detectorPath, jstring imgPath) {

    const string dStr = jstring2string(env, detectorPath);
    const string iStr = jstring2string(env, imgPath);
    const char *dPath = env->GetStringUTFChars(detectorPath, NULL);
    const char *iPath = env->GetStringUTFChars(imgPath, JNI_FALSE);


    ifstream fin(iPath, ios::binary);
    if (!fin) {
//        cout << "Can't find a trained object detector file object_detector.svm. " << endl;
//        cout << "You need to train one using the -t option." << endl;
//        cout << "\nTry the -h option for more information." << endl;
        LOGD("### JNI: Can't find a trained object detector file from %s", dPath);
        return EXIT_FAILURE;

    }
    object_detector<image_scanner_type> detector;
    deserialize(detector, fin);

    env->ReleaseStringUTFChars(detectorPath, dPath);
    env->ReleaseStringUTFChars(imgPath, iPath);

    return EXIT_SUCCESS;

//    net_type net;
//    shape_predictor sp;
    // You can get this file from http://dlib.net/files/mmod_front_and_rear_end_vehicle_detector.dat.bz2
    // This network was produced by the dnn_mmod_train_find_cars_ex.cpp example program.
    // As you can see, the file also includes a separately trained shape_predictor.  To see
    // a generic example of how to train those refer to train_shape_predictor_ex.cpp.
//    deserialize("../detector_vacas_nose_pontos.dat") >> net >> sp;
//    array2d<rgb_pixel> img;
    // Now load the image file into our image.  If something is wrong then
    // load_image() will throw an exception.  Also, if you linked with libpng
    // and libjpeg then load_image() can load PNG and JPEG files in addition
    // to BMP files.
//    load_image(img, "src/main/java/images/a2.jpg")
//    load_image(img, "../a2.jpg");
//    for (auto&& d : net(img))
//    {
//        // We use a shape_predictor to refine the exact shape and location of the detection
//        // box.  This shape_predictor is trained to simply output the 4 corner points of
//        // the box.  So all we do is make a rectangle that tightly contains those 4 points
//        // and that rectangle is our refined detection position.
//        auto fd = sp(img,d);
//        rectangle rect;
//        for (unsigned long j = 0; j < fd.num_parts(); ++j)
//            rect += fd.part(j);
//
//        LOGD("### JNI: %s",d.label.c_str());
//        LOGD("### JNI: %lu",fd.get_rect().area());
//    }
//    dlib::array<array2d<unsigned char> > images;
//    std::vector<std::vector<rectangle> > object_locations, ignore;
//
//    ignore = load_image_dataset(images, object_locations, iStr);
//    LOGD("### JNI: Number of images loaded: %zu", images.size());
//
//    // Upsample images if the user asked us to do that.
//    upsample_image_dataset<pyramid_down<2> >(images, object_locations, ignore);
//
//    image_scanner_type scanner;
//    structural_object_detection_trainer<image_scanner_type> trainer(scanner);
//
//    trainer.set_c(1);
//    trainer.set_epsilon(1);
// --------------------------------------------------------------------------------
// Now make sure all the boxes are obtainable by the scanner.
//    std::vector<std::vector<rectangle> > removed;
//    removed = remove_unobtainable_rectangles(trainer, images, object_locations);
//    // if we weren't able to get all the boxes to match then throw an error
//    test_object_detection_function(detector, images, object_locations, ignore);
//
//    // shuffle the order of the training images
//    randomize_samples(images, object_locations);
//    cross_validate_object_detection_trainer(trainer, images, object_locations, ignore, 1);
// --------------------------------------------------------------------------------
//    dlib::array<array2d<unsigned char> > images;
//    ifstream fin(dPath, ios::binary);
//    if (!fin) {
//
//        cout << "Can't find a trained object detector file object_detector.svm. " << endl;
//        cout << "You need to train one using the -t option." << endl;
//        cout << "\nTry the -h option for more information." << endl;
//        LOGD("###### JNI: Can't find a trained object detector file object_detector.svm from %s", dPath);
//        return EXIT_FAILURE;
//
//    }
//    deserialize(detector, fin);
// --------------------------------------------------------------------------------
//    object_detector<image_scanner_type> detector;
//    serialize(dPath) << detector;
//    object_detector<image_scanner_type> detector;
//
//    std::vector<object_detector<image_scanner_type>> detectors(1);
//    deserialize(dStr) >> detectors[0];
//
//    std::vector<dlib::rect_detection> detections;
//    dlib::evaluate_detectors(detectors, imgPath, detections);
//
//    for (auto & det: detections) {
//        det.rect; // found rectanglea comprar
//        det.weight_index; // detector index in vector (to indentify object class)
//    }
// --------------------------------------------------------------------------------
//    try{
//    dlib::deserialize(dStr) >> detector;
//    } catch (dlib::serialization_error &e) {
//        LOGD("JNI: failed to model -> %s", e.what());
//    }
// --------------------------------------------------------------------------------
//    std::vector<std::vector<rectangle> > object_locations, ignore;
//
//    cout << "JNI: Loading image dataset from metadata file " << iPath << endl;
//    LOGD("JNI: Loading image dataset from metadata file %s", iPath);
//    directory test(iPath);
//    ignore = load_image_dataset(detector,object_locations,test);
//    cout << "JNI: Number of images loaded: " << images.size() << endl;
//    LOGD("JNI: Number of images loaded %s", images.size());
//    // Upsample images if the user asked us to do that.
//    for (unsigned long i = 0; i < upsample_amount; ++i)
//        upsample_image_dataset<pyramid_down<2> >(images, object_locations, ignore);
//
//    if (parser.option("test"))
//    {
//        cout << "Testing detector on data..." << endl;
//        cout << "Results (precision,recall,AP): " << test_object_detection_function(detector, images, object_locations, ignore) << endl;
//        return EXIT_SUCCESS;
//    }
}

// ----------------------------------------------------------------------------------------




